package com.shankar.bitbucket.util;

import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

public class ClientResources {
	@NestedConfigurationProperty
	private ClientCredentialsResourceDetails  client = new ClientCredentialsResourceDetails ();

	@NestedConfigurationProperty
	private ResourceServerProperties resource = new ResourceServerProperties();

	public ClientCredentialsResourceDetails  getClient() {
		return client;
	}

	public ResourceServerProperties getResource() {
		return resource;
	}
}
