package com.shankar.bitbucket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import com.shankar.bitbucket.util.ClientResources;

@Configuration
@EnableOAuth2Client
@EnableWebSecurity
public class SecurityConfiguration {

	@Autowired
	OAuth2ClientContext oauth2ClientContext;

	@Bean
	@ConfigurationProperties("bitbucket")
	public ClientResources bitbucket() {
		return new ClientResources();
	}

	@Bean
	protected OAuth2ProtectedResourceDetails resource() {
		return bitbucket().getClient();
	}

	@Bean
	public OAuth2RestOperations restTemplate() {
		return new OAuth2RestTemplate(resource(), oauth2ClientContext);
	}

}
