package com.shankar.bitbucket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shankar.bitbucket.util.ClientResources;

@RestController
public class HelloController {
	
	@Autowired
	private OAuth2RestOperations oauth2RestOperations;
	
	@Autowired
	private ClientResources clientresources;

	@RequestMapping("/user")
	public String getUser() {
		ResponseEntity<String> response =  oauth2RestOperations.getForEntity(clientresources.getResource().getUserInfoUri()+"/user", String.class);
		return response.getBody().toString();
	}
	
	@GetMapping("/repositories")
	public String getIssue( ) {
		ResponseEntity<String> forEntity = oauth2RestOperations.getForEntity(clientresources.getResource().getUserInfoUri()+"/repositories/shankar389", String.class); 
	    return forEntity.getBody().toString();
	}

}
